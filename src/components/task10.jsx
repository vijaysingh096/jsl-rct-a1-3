import React, { Component } from "react";
class Task10 extends Component {
  state = {
    codeKey: ["A", "B", "C", 1, 2, 3],
    codeSoFar: "",
    codeArr: [],
  };
  makeCode = (w) => {
    let s1 = { ...this.state };
    s1.codeSoFar = s1.codeSoFar + w;
    this.setState(s1);
  };
  addNewCode = () => {
    let s1 = { ...this.state };
    let code = s1.codeArr.find((c) => c === s1.codeSoFar);
    if (code) {
      window.alert("code already exists");
    } else {
      s1.codeArr.push(s1.codeSoFar);
      s1.codeSoFar = "";
    }
    this.setState(s1);
  };
  clearCode = () => {
    let s1 = { ...this.state };
    s1.codeSoFar = "";
    this.setState(s1);
  };
  render() {
    let { codeKey, codeSoFar, codeArr } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <h2>Create New Code</h2>
          <h6>Code so far : {codeSoFar}</h6>
          {codeKey.map((w) => {
            return (
              <button
                className="btn btn-warning m-2"
                onClick={() => this.makeCode(w)}
              >
                {w}
              </button>
            );
          })}
          <br />
          <button
            className="btn btn-primary m-2"
            onClick={() => this.addNewCode()}
          >
            Add New Code
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.clearCode()}
          >
            Clear Code
          </button>
          <h2>List of Code Created</h2>
          <ul>
            {codeArr.map((c) => {
              return <li>{c}</li>;
            })}
          </ul>
        </div>
      </React.Fragment>
    );
  }
}
export default Task10;
