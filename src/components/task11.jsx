import React, { Component } from "react";
class Task11 extends Component {
  state = {
    codeType: -1,
    codeTypes: ["ABC123", "0-9", "MN012"],
    codeMaker: [
      ["A", "B", "C", 1, 2, 3],
      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
      ["M", "N", 0, 1, 2],
    ],
    newCode: "",
    codeArr: [],
  };

  showCodeMaker = (index) => {
    let s1 = { ...this.state };
    s1.codeType = index;
    this.setState(s1);
  };

  makeCode = (w) => {
    let s1 = { ...this.state };
    s1.newCode = s1.newCode + w;
    this.setState(s1);
  };

  addNewCode = () => {
    let s1 = { ...this.state };
    let code = s1.codeArr.find((c) => c === s1.newCode);
    if (code) {
      window.alert("code already exists");
    } else {
      s1.codeArr.push(s1.newCode);
      s1.codeType = -1;
      s1.newCode = "";
    }
    this.setState(s1);
  };

  clearCode = () => {
    let s1 = { ...this.state };
    s1.codeType = -1;
    s1.newCode = "";
    this.setState(s1);
  };
  render() {
    let { codeType, codeTypes, codeMaker, newCode, codeArr } = this.state;

    return (
      <React.Fragment>
        <div className="container">
          <h2>Create New Code</h2>

          {codeType >= 0 ? (
            <>
              <h6>Code Type : {codeTypes[codeType]}</h6>
              <h6>Code so far : {newCode}</h6>

              {codeMaker[codeType].map((w) => {
                return (
                  <button
                    className="btn btn-warning m-2"
                    onClick={() => this.makeCode(w)}
                  >
                    {w}
                  </button>
                );
              })}
              <br />
              <button
                className="btn btn-primary m-2"
                onClick={() => this.addNewCode()}
              >
                Add New Code
              </button>
              <button
                className="btn btn-primary m-2"
                onClick={() => this.clearCode()}
              >
                Clear Code
              </button>
            </>
          ) : (
            <>
              {codeTypes.map((w, index) => {
                return (
                  <button
                    className="btn btn-primary m-2"
                    onClick={() => this.showCodeMaker(index)}
                  >
                    {w}
                  </button>
                );
              })}
            </>
          )}
          <h2>List of Code Created</h2>
          <ul>
            {codeArr.map((c) => {
              return <li>{c}</li>;
            })}
          </ul>
        </div>
      </React.Fragment>
    );
  }
}
export default Task11;
