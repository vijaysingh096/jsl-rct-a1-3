import React, { Component } from "react";
class Task1a extends Component {
  state = {
    txt1: "No button clicked",
    count1: 0,
    txt2: "No button clicked",
    count2: 0,
  };
  clickedABC = () => {
    let s1 = { ...this.state };
    s1.txt1 = "ABC";
    s1.count1++;
    this.setState(s1);
  };
  clicked123 = () => {
    let s1 = { ...this.state };
    s1.txt1 = "123";
    s1.count1++;
    this.setState(s1);
  };
  // or using one function
  clickedButton = (str) => {
    let s1 = { ...this.state };
    s1.txt2 = str;
    s1.count2++;
    this.setState(s1);
  };

  render() {
    let { txt1, count1, txt2, count2 } = this.state;
    return (
      <React.Fragment>
        <div className="container text-center">
          <button
            className="btn btn-primary m-2"
            onClick={() => this.clickedABC()}
          >
            ABC
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.clicked123()}
          >
            123
          </button>
          <br />
          Text: {txt1}
          <br />
          Count:{count1}
          <hr />
          <h6>using one function</h6>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.clickedButton("ABC")}
          >
            ABC
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.clickedButton("123")}
          >
            123
          </button>
          <br />
          Text: {txt2}
          <br />
          Count:{count2}
          <hr />
        </div>
      </React.Fragment>
    );
  }
}
export default Task1a;
