import React, { Component } from "react";
class Task1b extends Component {
  state = {
    person: [
      {
        name: "Jack",
        age: 25,
        skills: "Javascript,React",
        email: "jack@email.com",
        mobile: "2345441414",
      },
      {
        name: "Anna",
        age: 29,
        skills: "Node.js,MongoDB",
        email: "Anna@email.com",
        mobile: "781120053",
      },
      {
        name: "Steve",
        age: 31,
        skills: "Android App Development",
        email: "steve@email.com",
        mobile: "9678124493",
      },
    ],
    indexPerson: -1,
    showContact: false,
  };

  setIndex = (index) => {
    let s1 = { ...this.state };
    s1.indexPerson = index;
    s1.showContact=false;
    this.setState(s1);
  };

  showDetail = () => {
    const { person, indexPerson, showContact } = this.state;
    return (
      <React.Fragment>
        Name: {person[indexPerson].name}
        <br />
        Age: {person[indexPerson].age}
        <br />
        Skills: {person[indexPerson].skills}
        <br />
        <button
          className="btn btn-primary m-2"
          onClick={() => this.showContactDetail()}
        >
          Contact Info
        </button>
        <br />
        {showContact ? (
          <React.Fragment>
            Email: {person[indexPerson].email}
            <br />
            Mobile: {person[indexPerson].mobile}
          </React.Fragment>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  };

  showContactDetail = () => {
    let s1 = { ...this.state };
    s1.showContact = true;
    this.setState(s1);
  };
  render() {
    const { person, indexPerson } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          {person.map((p, index) => {
            let { name, age, skills, email, mobile } = p;
            return (
              <React.Fragment>
                <button
                  className="btn btn-primary m-2"
                  onClick={() => this.setIndex(index)}
                >
                  {name}
                </button>
              </React.Fragment>
            );
          })}
          <hr />
          {indexPerson >= 0 ? this.showDetail() : ""}
        </div>
      </React.Fragment>
    );
  }
}
export default Task1b;
