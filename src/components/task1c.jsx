import React, { Component } from "react";

class Task1c extends Component {
  state = {
    list1: ["Bob", "Tim", "Julia", "Steve", "Edwards", "George", "Kathy"],
    list2: [],
  };
  list1Click = (index) => {
    let s1 = { ...this.state };
    let name = s1.list1.splice(index, 1);
    s1.list2.push(name);
    this.setState(s1);
  };

  list2Click = (index) => {
    let s1 = { ...this.state };
    let name = s1.list2.splice(index, 1);
    s1.list1.push(name);
    this.setState(s1);
  };

  render() {
    let { list1, list2 } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-4 border">
              <h4>List 1</h4>
              <hr />
              {list1.map((l1, index) => {
                return (
                  <React.Fragment>
                    <span onClick={() => this.list1Click(index)}>
                      {l1}
                      <br />
                    </span>
                  </React.Fragment>
                );
              })}
            </div>

            <div className="col-4 border">
              <h4>List 2</h4>
              <hr />
              {list2.map((l2, index) => {
                return (
                  <React.Fragment>
                    {l2}
                    <button
                      className="btn btn-danger btn-sm mx-3"
                      onClick={() => this.list2Click(index)}
                    >
                      X
                    </button>{" "}
                    <br />
                  </React.Fragment>
                );
              })}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Task1c;
