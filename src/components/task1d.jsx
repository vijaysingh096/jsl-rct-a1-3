import React, { Component } from "react";
class Task1d extends Component {
  state = {
    products: [
      { name: "Pepsi", price: 20 },
      { name: "Dairy Milk", price: 40 },
      { name: "Maggi", price: 15 },
      { name: "Snickers", price: 50 },
      { name: "Nescafe", price: 100 },
    ],
    cart: [],
  };

  showCart = () => {
    const { cart } = this.state;
    if (cart.length === 0) return <h3>Cart is Empty</h3>;
    return (<>
        <h3>Cart </h3>
        <hr/>
      <ul>
        {cart.map((item) => {
          let { name, price, qty } = item;
          return (
            <li>
              {name}, Price= {price}, qty= {qty}{" "}
            </li>
          );
        })}
      </ul>
      <hr/>
      </>
    );
  };
  showProduct = () => {
    const { products } = this.state;
    return (
      <React.Fragment>
        <div className="row bg-dark text-whiet">
          <div className="col-4">Name</div>
          <div className="col-4">Price</div>
          <div className="col-4"></div>
        </div>
        {products.map((prod, index) => {
          let { name, price } = prod;
          return (
            <React.Fragment>
              <div className="row border">
                <div className="col-4">{name}</div>
                <div className="col-4">{price} </div>
                <div className="col-4">
                  <button
                    className="btn btn-primary btn-sm"
                    onClick={() => this.addToCart(index)}
                  >
                    Add to Cart
                  </button>{" "}
                </div>
              </div>
            </React.Fragment>
          );
        })}
      </React.Fragment>
    );
  };

  addToCart = (index) => {
    let s1 = { ...this.state };
    let pr = s1.products[index];
    let x1 = s1.cart.find((p1) => p1.name === pr.name);
    x1 ? (x1.qty = x1.qty + 1) : s1.cart.push({ ...pr, qty: 1 });
    this.setState(s1);
  };

  render() {
    return (
      <React.Fragment>
        <div className="container">
          {this.showCart()}
          {this.showProduct()}
        </div>
      </React.Fragment>
    );
  }
}
export default Task1d;
