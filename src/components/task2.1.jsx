import React,{Component} from "react";
class Task2_1 extends Component{
state={
    number:0
}
add1=()=>{
    let s1={...this.state};
    s1.number=s1.number+1;
    this.setState(s1)
}
add10=()=>{
    let s1={...this.state};
    s1.number=s1.number+10;
    this.setState(s1)
}
add100=()=>{
    let s1={...this.state};
    s1.number=s1.number+100;
    this.setState(s1)
}
render(){
    let {number}=this.state;
    return(
        <React.Fragment>
            <div className="container">
                <button className="btn btn-primary m-2" onClick={()=>this.add1()}>1</button>
                <button className="btn btn-primary m-2" onClick={()=>this.add10()}>10</button>
                <button className="btn btn-primary m-2" onClick={()=>this.add100()}>100</button><br/>
                <hr/>
               <h6>Number =  {number}</h6> 
            </div>
        </React.Fragment>
    )
}
}
export default Task2_1;