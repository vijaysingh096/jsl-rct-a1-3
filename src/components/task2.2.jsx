import React,{Component} from "react";
class Task2_2 extends Component{
state={
    number:0
}
add=(num)=>{
    let s1={...this.state};
    s1.number=s1.number+num;
    this.setState(s1)
}

render(){
    let {number}=this.state;
    return(
        <React.Fragment>
            <div className="container">
                <button className="btn btn-primary m-2" onClick={()=>this.add(1)}>1</button>
                <button className="btn btn-primary m-2" onClick={()=>this.add(10)}>10</button>
                <button className="btn btn-primary m-2" onClick={()=>this.add(100)}>100</button><br/>
                <hr/>
               <h6>Number =  {number}</h6> 
            </div>
        </React.Fragment>
    )
}
}
export default Task2_2;