import React, { Component } from "react";
class Task3_1 extends Component {
  state = {
    txt: "",
  };
  addA = () => {
    let s1 = { ...this.state };
    s1.txt = s1.txt + "A";
    this.setState(s1);
  };
  addB = () => {
    let s1 = { ...this.state };
    s1.txt = s1.txt + "B";
    this.setState(s1);
  };
  addC = () => {
    let s1 = { ...this.state };
    s1.txt = s1.txt + "C";
    this.setState(s1);
  };

  addD = () => {
    let s1 = { ...this.state };
    s1.txt = s1.txt + "D";
    this.setState(s1);
  };

  render() {
    let { txt } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <button className="btn btn-primary m-2" onClick={() => this.addA()}>
            A
          </button>
          <button className="btn btn-primary m-2" onClick={() => this.addB()}>
            B
          </button>
          <button className="btn btn-primary m-2" onClick={() => this.addC()}>
            C
          </button>
          <button className="btn btn-primary m-2" onClick={() => this.addD()}>
            D
          </button>
          <br />
          <hr />
          <h6>Text = {txt}</h6>
        </div>
      </React.Fragment>
    );
  }
}
export default Task3_1;
