import React, { Component } from "react";
class Task3_3 extends Component {
  state = {
    txt: "",
  };
  add = (str) => {
    let s1 = { ...this.state };
    s1.txt = s1.txt + str;
    this.setState(s1);
  };
backSpace=()=>{
    let s1 = { ...this.state };
    s1.txt =s1.txt.slice(0,-1) 
    this.setState(s1);
}
clear=()=>{
    let s1 = { ...this.state };
    s1.txt =""
    this.setState(s1);
}
  render() {
    let { txt } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <button className="btn btn-primary m-2" onClick={() => this.add("A")}>
            A
          </button>
          <button className="btn btn-primary m-2" onClick={() => this.add("B")}>
            B
          </button>
          <button className="btn btn-primary m-2" onClick={() => this.add("C")}>
            C
          </button>
          <button className="btn btn-primary m-2" onClick={() => this.add("D")}>
            D
          </button>
          <br />
          <hr />
          <h6>Text = {txt}</h6> <br />
          <button className="btn btn-primary m-2" onClick={() => this.backSpace()}>
            Back Space
          </button>
          <button className="btn btn-primary m-2" onClick={() => this.clear()}>
            Clear
          </button>
         
        </div>
      </React.Fragment>
    );
  }
}
export default Task3_3;
