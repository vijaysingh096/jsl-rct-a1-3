import React, { Component } from "react";
class Task4_1 extends Component {
  state = {
    scorA: 0,
    scorB: 0,
  };

  add3PointA = () => {
    let s1 = { ...this.state };
    s1.scorA = s1.scorA + 3;
    this.setState(s1);
  };
  add2PointA = () => {
    let s1 = { ...this.state };
    s1.scorA = s1.scorA + 2;
    this.setState(s1);
  };
  add1PointA = () => {
    let s1 = { ...this.state };
    s1.scorA = s1.scorA + 1;
    this.setState(s1);
  };
  add3PointB = () => {
    let s1 = { ...this.state };
    s1.scorB = s1.scorB + 3;
    this.setState(s1);
  };
  add2PointB = () => {
    let s1 = { ...this.state };
    s1.scorB = s1.scorB + 2;
    this.setState(s1);
  };
  add1PointB = () => {
    let s1 = { ...this.state };
    s1.scorB = s1.scorB + 1;
    this.setState(s1);
  };

  resetScore=()=>{
    let s1 = { ...this.state };
    s1.scorA = 0;
    s1.scorB = 0;
    this.setState(s1);
  }
  render() {
    let { scorA, scorB } = this.state;
    return (
      <React.Fragment>
        <div className="containern">
          <div className="row  text-center">
            <div className="col-6 border">
              <h6 className="text-muted "> Team A</h6>
              <h3 className="display-3">{scorA} </h3>
              <button
                className="btn btn-warning m-2"
                onClick={() => this.add3PointA()}
              >
                +3 POINTS
              </button>
              <br />
              <button
                className="btn btn-warning m-2"
                onClick={() => this.add2PointA()}
              >
                +2 POINTS
              </button>
              <br />
              <button
                className="btn btn-warning m-2"
                onClick={() => this.add1PointA()}
              >
                FREE THROW
              </button>
              <br />
            </div>

            <div className="col-6 border">
              <h6 className="text-muted "> Team B</h6>
              <h3 className="display-3">{scorB} </h3>
              <button
                className="btn btn-warning m-2"
                onClick={() => this.add3PointB()}
              >
                +3 POINTS
              </button>
              <br />
              <button
                className="btn btn-warning m-2"
                onClick={() => this.add2PointB()}
              >
                +2 POINTS
              </button>
              <br />
              <button
                className="btn btn-warning m-2"
                onClick={() => this.add1PointB()}
              >
                FREE THROW
              </button>
            </div>
          </div>
          <div className="row border  text-center">
            <div className=" col">
              <button
                className="btn btn-warning m-2"
                onClick={() => this.resetScore()}
              >
                RESET
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Task4_1;
