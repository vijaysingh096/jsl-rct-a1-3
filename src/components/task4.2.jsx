import React, { Component } from "react";
class Task4_2 extends Component {
  state = {
    scorA: 0,
    scorB: 0,
  };

  addPointA = (num) => {
    let s1 = { ...this.state };
    s1.scorA = s1.scorA + num;
    this.setState(s1);
  };

  addPointB = (num) => {
    let s1 = { ...this.state };
    s1.scorB = s1.scorB + num;
    this.setState(s1);
  };

  resetScore = () => {
    let s1 = { ...this.state };
    s1.scorA = 0;
    s1.scorB = 0;
    this.setState(s1);
  };
  render() {
    let { scorA, scorB } = this.state;
    return (
      <React.Fragment>
        <div className="containern">
        <h2  className="  text-center">Basketball Scoring</h2>
          <div className="row  text-center">
            <div className="col-6 border">
              <h6 className="text-muted "> Team A</h6>
              <h3 className="display-3">{scorA} </h3>
              <button
                className="btn btn-warning m-2"
                onClick={() => this.addPointA(3)}
              >
                +3 POINTS
              </button>
              <br />
              <button
                className="btn btn-warning m-2"
                onClick={() => this.addPointA(2)}
              >
                +2 POINTS
              </button>
              <br />
              <button
                className="btn btn-warning m-2"
                onClick={() => this.addPointA(1)}
              >
                FREE THROW
              </button>
              <br />
            </div>

            <div className="col-6 border">
              <h6 className="text-muted "> Team B</h6>
              <h3 className="display-3">{scorB} </h3>
              <button
                className="btn btn-warning m-2"
                onClick={() => this.addPointB(3)}
              >
                +3 POINTS
              </button>
              <br />
              <button
                className="btn btn-warning m-2"
                onClick={() => this.addPointB(2)}
              >
                +2 POINTS
              </button>
              <br />
              <button
                className="btn btn-warning m-2"
                onClick={() => this.addPointB(1)}
              >
                FREE THROW
              </button>
            </div>
          </div>
          <div className="row border  text-center">
            <div className=" col">
              <button
                className="btn btn-warning m-2"
                onClick={() => this.resetScore()}
              >
                RESET
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Task4_2;
