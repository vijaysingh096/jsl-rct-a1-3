import React, { Component } from "react";
class Task5_1 extends Component {
  state = {
    numbers: [1, 3, 5, 7],
    expression: "",
    calcValue: 0,
  };

  addNumber = (n1) => {
    let s1 = { ...this.state };
    s1.expression = s1.expression.length > 0 ? s1.expression + "+" + n1 : s1.expression+n1;
    s1.calcValue = s1.calcValue + n1;
    this.setState(s1);
  };

  render() {
    let { numbers, expression, calcValue } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          {numbers.map((n1) => {
            return (
              <button
                className="btn btn-primary m-2"
                onClick={() => this.addNumber(n1)}
              >
                {n1}
              </button>
            );
          })}
          <p>Expression = {expression}</p>
          <p>Calculated Value = {calcValue}</p>
        </div>
      </React.Fragment>
    );
  }
}
export default Task5_1;
