import React, { Component } from "react";
class Task5_2 extends Component {
  state = {
    numbers: [1, 3, 5, 7],
    expression: "",
    calcValue: 0,
    operator: "",
  };
  add = () => {
    let s1 = { ...this.state };
    s1.operator = "+";
    s1.expression = "";
    s1.calcValue = 0;
    this.setState(s1);
  };
  multiply = () => {
    let s1 = { ...this.state };
    s1.operator = "*";
    s1.expression = "";
    s1.calcValue = 1;
    this.setState(s1);
  };

  addNumber = (n1) => {
    let s1 = { ...this.state };
    if (s1.operator === "+") {
      s1.expression =
        s1.expression.length > 0
          ? s1.expression + s1.operator + n1
          : s1.expression + n1;
      s1.calcValue = s1.calcValue + n1;
    }
    if (s1.operator === "*") {
      s1.expression =
        s1.expression.length > 0
          ? s1.expression + s1.operator + n1
          : s1.expression + n1;
      s1.calcValue = s1.calcValue * n1;
    }
    this.setState(s1);
  };

  render() {
    let { numbers, expression, calcValue, operator } = this.state;

    return (
      <React.Fragment>
        <div className="container">
          Operator :{" "}
          <button className="btn btn-primary m-2" onClick={() => this.add()}>
            Add
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.multiply()}
          >
            Multiply
          </button>
          <br />
          <hr />
          {numbers.map((n1) => {
            return (
              <button
                className="btn btn-primary m-2"
                onClick={() => this.addNumber(n1)}
              >
                {n1}
              </button>
            );
          })}
          <p>Operator = {operator}</p>
          <p>Expression = {expression}</p>
          <p>Calculated Value = {calcValue}</p>
        </div>
      </React.Fragment>
    );
  }
}
export default Task5_2;
