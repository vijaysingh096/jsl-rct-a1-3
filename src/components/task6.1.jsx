import React, { Component } from "react";
class Task6_1 extends Component {
  state = {
    products: [
      { name: "Pepsi", sales: 10 },
      { name: "Coke", sales: 7 },
      { name: "Maggi", sales: 0 },
      { name: "Gems", sales: 15 },
      { name: "5Star", sales: 0 },
    ],
  };
  render() {
    let { products } = this.state;
    return (
      <React.Fragment>
        <div className="container">
        <h3>Sales Details</h3>
          <ul>
            {products.map((prod, index) => {
              let { name, sales } = prod;
              return (
                <li>
                  Name : {name}, Sales : {sales}{" "}
                  <button
                    className="btn btn-primary m-2"
                    onClick={() => this.sellProd(index)}
                  >Sale</button>
                </li>
              );
            })}
          </ul>
        </div>
      </React.Fragment>
    );
  }
}
export default Task6_1;
