import React, { Component } from "react";
class Task6_4 extends Component {
  state = {
    products: [
      { name: "Liril", sales: 0 },
      { name: "Lux", sales: 0 },
      { name: "Dove", sales: 22 },
      { name: "Pears", sales: 25 },
    ],
    totalSale: 0,
    bestSale: 0,
  };
  totalSale = () => {
    let s1 = { ...this.state };
    let TotSal = s1.products.reduce((acc, curr) => (acc = acc + curr.sales), 0);
    s1.totalSale = TotSal;
    this.setState(s1);
  };

  bestSale = () => {
    let s1 = { ...this.state };
    let bestSal = s1.products.reduce(
      (acc, curr) => (acc = acc > curr.sales ? acc : curr.sales),
      0
    );
    s1.bestSale = bestSal;
    this.setState(s1);
  };
  render() {
    let { products, totalSale, bestSale } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <h3>Sales Details</h3>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.totalSale()}
          >
            Total Sale
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.bestSale()}
          >
            Best Sale
          </button>
          <ul>
            {products.map((prod, index) => {
              let { name, sales } = prod;

              let li =
                sales > 0 ? (
                  <li>
                    Name : {name}, Sales : {sales}{" "}
                    <button
                      className="btn btn-primary m-2"
                      onClick={() => this.saleProd(index)}
                    >
                      Sale
                    </button>
                    <button
                      className="btn btn-danger m-2"
                      onClick={() => this.returnProdProd(index)}
                    >
                      Return
                    </button>
                  </li>
                ) : (
                  <li>
                    Name : {name}, Sales : {sales}{" "}
                    <button
                      className="btn btn-primary m-2"
                      onClick={() => this.saleProd(index)}
                    >
                      Sale
                    </button>
                  </li>
                );
              return li;
            })}
          </ul>
          <br />
          Total Sales ={totalSale}
          <br />
          Best Sales ={bestSale}
        </div>
      </React.Fragment>
    );
  }
}
export default Task6_4;
