import React, { Component } from "react";
class Task7_1 extends Component {
  state = {
    products: [
      { name: "Perk", price: 10, sales: 7 },
      { name: "5Star", price: 15, sales: 9 },
      { name: "Pepsi", price: 20, sales: 20 },
      { name: "Maggi", price: 12, sales: 15 },
      { name: "Coke", price: 20, sales: 50 },
      { name: "Lindt", price: 80, sales: 4 },
    ],
  };
  sort = (colNo) => {
    let s1 = { ...this.state };
    console.log(colNo);
    switch (colNo) {
      case 0:
        s1.products = s1.products.sort((p1, p2) => p1.name.localeCompare(p2.name));
        break;
      case 1:
        s1.products = s1.products.sort((p1, p2) => p1.price - p2.price);
        break;
      case 2:
        s1.products = s1.products.sort((p1, p2) => p1.sales - p2.sales);
        break;
      case 3:
        s1.products = s1.products.sort(
          (p1, p2) => p1.price * p1.sales - p2.price * p2.sales
        );
        break;
    }
    this.setState(s1);
  };

  render() {
    let { products } = this.state;

    return (
      <React.Fragment>
        <div className="container">
          <div className="row border bg-dark text-white">
            <div className="col-3" onClick={() => this.sort(0)}>
              Name
            </div>
            <div className="col-3" onClick={() => this.sort(1)}>
              Price
            </div>
            <div className="col-3" onClick={() => this.sort(2)}>
              Sales
            </div>
            <div className="col-3" onClick={() => this.sort(3)}>
              Value
            </div>
          </div>
          {products.map((prod, index) => {
            let { name, price, sales } = prod;
            return (
              <React.Fragment>
                <div className="row border ">
                  <div className="col-3">{name}</div>
                  <div className="col-3">{price} </div>
                  <div className="col-3">{sales} </div>
                  <div className="col-3">{price * sales} </div>
                </div>
              </React.Fragment>
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}
export default Task7_1;
