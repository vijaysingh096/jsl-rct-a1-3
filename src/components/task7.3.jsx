import React, { Component } from "react";
class Task7_3 extends Component {
  state = {
    products: [
      { name: "Perk", price: 10, sales: 7 },
      { name: "5Star", price: 15, sales: 9 },
      { name: "Pepsi", price: 20, sales: 20 },
      { name: "Maggi", price: 12, sales: 15 },
      { name: "Coke", price: 20, sales: 50 },
      { name: "Lindt", price: 80, sales: 4 },
    ],
    txt: "Not Sorted",
  };
  sort = (colNo) => {
    let s1 = { ...this.state };
    console.log(colNo);
    switch (colNo) {
      case 0:
        s1.products = s1.products.sort((p1, p2) =>
          p1.name.localeCompare(p2.name)
        );
        s1.txt = "Sorted by Name";
        break;
      case 1:
        s1.products = s1.products.sort((p1, p2) => p1.price - p2.price);
        s1.txt = "Sorted by Price";
        break;
      case 2:
        s1.products = s1.products.sort((p1, p2) => p1.sales - p2.sales);
        s1.txt = "Sorted by Sales";
        break;
      case 3:
        s1.products = s1.products.sort(
          (p1, p2) => p1.price * p1.sales - p2.price * p2.sales
        );
        s1.txt = "Sorted by Value";
        break;
    }
    this.setState(s1);
  };

  filterPrice = () => {
    let s1 = { ...this.state };
    s1.products = s1.products.filter((p1) => p1.price >= 15);
    s1.txt = "Filter : Price >=15 ";
    this.setState(s1);
  };

  filterSales = () => {
    let s1 = { ...this.state };
    s1.products = s1.products.filter((p1) => p1.sales >= 10);
    s1.txt = "Filter : Sales >=10 ";
    this.setState(s1);
  };

  filterValue = () => {
    let s1 = { ...this.state };
    s1.products = s1.products.filter((p1) =>p1.price*p1.sales >= 100);
    s1.txt = "Filter : Value >=100 ";
    this.setState(s1);
  };
  render() {
    let { products, txt } = this.state;

    return (
      <React.Fragment>
        <div className="container">
          <h2>{txt}</h2>
          <br />
          <hr />

          <button
            className="btn btn-primary m-2"
            onClick={() => this.filterPrice()}
          >
            Price{">="}15
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.filterSales()}
          >
            Sales{">="}10
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.filterValue()}
          >
            Value{">="}100
          </button>

          <div className="row border bg-dark text-white">
            <div className="col-3" onClick={() => this.sort(0)}>
              Name
            </div>
            <div className="col-3" onClick={() => this.sort(1)}>
              Price
            </div>
            <div className="col-3" onClick={() => this.sort(2)}>
              Sales
            </div>
            <div className="col-3" onClick={() => this.sort(3)}>
              Value
            </div>
          </div>
          {products.map((prod, index) => {
            let { name, price, sales } = prod;
            return (
              <React.Fragment>
                <div className="row border ">
                  <div className="col-3">{name}</div>
                  <div className="col-3">{price} </div>
                  <div className="col-3">{sales} </div>
                  <div className="col-3">{price * sales} </div>
                </div>
              </React.Fragment>
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}
export default Task7_3;
