import React, { Component } from "react";
class Task8 extends Component {
  state = {
    priceData: [
      { name: "Perk", category: "Food", oldPrice: 10, newPrice: 10 },
      { name: "5Star", category: "Food", oldPrice: 15, newPrice: 12 },
      { name: "Pepsi", category: "Drink", oldPrice: 20, newPrice: 22 },
      { name: "Maggi", category: "Food", oldPrice: 12, newPrice: 15 },
      { name: "Coke", category: "Drink", oldPrice: 20, newPrice: 20 },
      { name: "Gems", category: "Food", oldPrice: 10, newPrice: 10 },
      { name: "7Up", category: "Drink", oldPrice: 15, newPrice: 17 },
      { name: "Lindt", category: "Food", oldPrice: 80, newPrice: 90 },
      { name: "Nutties", category: "Food", oldPrice: 20, newPrice: 18 },
      { name: "Slice", category: "Drink", oldPrice: 18, newPrice: 16 },
    ],
    filter: "",
    txt: "Not Sorted",
  };
  sort = (colNo) => {
    let s1 = { ...this.state };
    console.log(colNo);
    switch (colNo) {
      case 0:
        s1.priceData = s1.priceData.sort((p1, p2) =>
          p1.name.localeCompare(p2.name)
        );
        s1.txt = "Sorted by Name";
        break;
      case 1:
        s1.priceData = s1.priceData.sort((p1, p2) =>
          p1.category.localeCompare(p2.category)
        );
        s1.txt = "Sorted by Category";
        break;
      case 2:
        s1.priceData = s1.priceData.sort((p1, p2) => p1.oldPrice - p2.oldPrice);
        s1.txt = "Sorted by Old Price";
        break;
      case 3:
        s1.priceData = s1.priceData.sort((p1, p2) => p1.newPrice - p2.newPrice);
        s1.txt = "Sorted by New Price";
        break;
    }
    this.setState(s1);
  }; 

  filterFood = () => {
    let s1 = { ...this.state };
    s1.filter="Food"
    s1.txt = "Category : Food ";
    this.setState(s1);
  };

  filterDrink = () => {
    let s1 = { ...this.state };
    s1.filter="Drink"
    s1.txt = "Category : Drink ";
    this.setState(s1);
  };

  filterIncrease = () => {
    let s1 = { ...this.state };
    s1.filter="Increase"
    s1.txt = "Price Increase ";
    this.setState(s1);
  };
  filterDecrease = () => {
    let s1 = { ...this.state };
    s1.filter="Decrease"
    s1.txt = "Price Decrease ";
    this.setState(s1);
  };

  filterSame = () => {
    let s1 = { ...this.state };
    s1.filter="Same"
    s1.txt = "Price Same ";
    this.setState(s1);
  };
  render() {
    let { priceData, txt, filter } = this.state;
    let showTable =
      filter === "Food"
        ? priceData.filter((p1) => p1.category === "Food")
        : filter === "Drink"
        ? priceData.filter((p1) => p1.category === "Drink")
        : filter === "Increase"
        ? priceData.filter((p1) => p1.oldPrice < p1.newPrice)
        : filter === "Decrease"
        ? priceData.filter((p1) => p1.oldPrice > p1.newPrice)
        : filter === "Same"
        ? priceData.filter((p1) => p1.oldPrice === p1.newPrice)
        : priceData;
    return (
      <React.Fragment>
        <div className="container">
          <h2>{txt}</h2>
          <br />
          <hr />

          <button
            className="btn btn-primary m-2"
            onClick={() => this.filterFood()}
          >
            Food
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.filterDrink()}
          >
            Drink
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.filterIncrease()}
          >
            Increase
          </button>

          <button
            className="btn btn-primary m-2"
            onClick={() => this.filterDecrease()}
          >
            Decrease
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.filterSame()}
          >
            Same
          </button>
          <div className="row border bg-dark text-white">
            <div className="col-3" onClick={() => this.sort(0)}>
              Name
            </div>
            <div className="col-3" onClick={() => this.sort(1)}>
              Category
            </div>
            <div className="col-3" onClick={() => this.sort(2)}>
              Old Price
            </div>
            <div className="col-3" onClick={() => this.sort(3)}>
              New Price
            </div>
          </div>
          {showTable.map((prod, index) => {
            let { name, category, oldPrice, newPrice } = prod;
            return (
              <React.Fragment>
                <div className="row border ">
                  <div className="col-3">{name}</div>
                  <div className="col-3">{category} </div>
                  <div className="col-3">{oldPrice} </div>
                  <div className="col-3">{newPrice} </div>
                </div>
              </React.Fragment>
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}
export default Task8;
