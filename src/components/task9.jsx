import React, { Component } from "react";
class Task9 extends Component {
  state = {
    movies: [
      { title: "Terminator", genre: "Action", stock: 6, rate: 2.5 },
      { title: "Die Hard", genre: "Action", stock: 5, rate: 2.5 },
      { title: "Get Out", genre: "Thriller", stock: 8, rate: 3.5 },
      { title: "Trip to Italy", genre: "Comedy", stock: 7, rate: 3.5 },
      { title: "Airplane", genre: "Comedy", stock: 7, rate: 3.5 },
      { title: "Wedding Crashers", genre: "Comedy", stock: 7, rate: 3.5 },
      { title: "Gone Girl", genre: "Thriller", stock: 7, rate: 4.5 },
      { title: "The Sixth Sense", genre: "Thriller", stock: 4, rate: 3.5 },
      { title: "The Avengers", genre: "Action", stock: 7, rate: 3.5 },
    ],
  };
  sort = (colNo) => {
    let s1 = { ...this.state };
    // console.log(colNo);
    switch (colNo) {
      case 0:
        s1.movies = s1.movies.sort((m1, m2) =>
          m1.title.localeCompare(m2.title)
        );
        break;
      case 1:
        s1.movies = s1.movies.sort((m1, m2) =>
          m1.genre.localeCompare(m2.genre)
        );
        break;
      case 2:
        s1.movies = s1.movies.sort((m1, m2) => m1.stock - m2.stock);
        break;
      case 3:
        s1.movies = s1.movies.sort((m1, m2) => m1.rate - m2.rate);
        break;
    }
    this.setState(s1);
  };
  deleteMovi = (index) => {
    let s1 = {...this.state};
    s1.movies.splice(index, 1);
    this.setState(s1)
  };
  render() {
    let { movies } = this.state;
    let noOfMovi =
      movies.length === 0
        ? "There is no movies"
        : "Showing " + movies.length + " movies";
    return (
      <React.Fragment>
        <div className="container">
          <h2>{noOfMovi}</h2><hr/>
          <div className="row border text-center">
            <div
              className="col-3 "
              onClick={() => this.sort(0)}
            >
              <h5> Title</h5>
            </div>
            <div className="col-2" onClick={() => this.sort(1)}>
              <h5> Genre</h5>
            </div>
            <div className="col-2" onClick={() => this.sort(2)}>
              <h5> Stock</h5>
            </div>
            <div className="col-2" onClick={() => this.sort(3)}>
              <h5> Rate</h5>
            </div>
            <div className="col-3"></div>
          </div>

          {movies.map((movi, index) => {
            let { title, genre, stock, rate } = movi;
            return (
              <React.Fragment>
                <div className="row border text-center">
                  <div className="col-3">{title}</div>
                  <div className="col-2">{genre} </div>
                  <div className="col-2">{stock} </div>
                  <div className="col-2">{rate} </div>
                  <div className="col-3">
                    {" "}
                    <button
                      className="btn btn-danger m-2"
                      onClick={() => this.deleteMovi(index)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </React.Fragment>
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}
export default Task9;
