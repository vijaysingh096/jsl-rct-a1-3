import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import Task1a from "./components/task1a";
import Task1b from "./components/task1b";
import Task1c from "./components/task1c";
import Task1d from "./components/task1d";
import Task2_1 from "./components/task2.1";
import Task2_2 from "./components/task2.2";
import Task3_1 from "./components/task3.1";
import Task3_2 from "./components/task3.2";
import Task3_3 from "./components/task3.3";
import Task4_1 from "./components/task4.1";
import Task4_2 from "./components/task4.2";
import Task4_3 from "./components/task4.3";
import Task5_1 from "./components/task5.1";
import Task5_2 from "./components/task5.2";
import Task6_1 from "./components/task6.1";
import Task6_2 from "./components/task6.2";
import Task6_3 from "./components/task6.3";
import Task6_4 from "./components/task6.4";
import Task7_1 from "./components/task7.1";
import Task7_2 from "./components/task7.2";
import Task7_3 from "./components/task7.3";
import Task8 from "./components/task8";
import Task9 from "./components/task9";
import Task10 from "./components/task10";
import Task11 from "./components/task11";

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <Task1a /> */}
    {/* <Task1b /> */}
    {/* <Task1c /> */}
    {/* <Task1d /> */}
    {/* <Task2_1 /> */}
    {/* <Task2_2 /> */}
    {/* <Task3_1 /> */}
    {/* <Task3_2 /> */}
    {/* <Task3_3 /> */}
    {/* <Task4_1 /> */}
    {/* <Task4_2 /> */}
    {/* <Task4_3 /> */}
    {/* <Task5_1 /> */}
    {/* <Task5_2 /> */}
    {/* <Task6_1 /> */}
    {/* <Task6_2 /> */}
    {/* <Task6_3 /> */}
    {/* <Task6_4 /> */}
    {/* <Task7_1 /> */}
    {/* <Task7_2 /> */}
    {/* <Task7_3 /> */}
    {/* <Task8 /> */}
    {/* <Task9 /> */}
    {/* <Task10 /> */}
    <Task11 />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
